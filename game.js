// Create HTML5 canvas and put it in the document.
var body;
var cvs = document.createElement("CANVAS");
var ctx = cvs.getContext("2d");
var dt, tt = performance.now(); // Time of previous frame.
var pressed = []; // Which keys are pressed at the moment.
var w, h, cx, cy; // Info about viewport.
var background0, background1, background2;
var player = new Player(0, 1);
var enemies = [];
var enemycount;
var flowers = [];
var flowercount, flowersdone;
var paused = false;
var draw = frameBegin;
var st, et, pt; // Start and end times + pause time.
var isBestTime = false;
var AudioContext = window.AudioContext || window.webkitAudioContext;
var actx = new AudioContext();
var globalgain = actx.createGain();
globalgain.connect(actx.destination);
var music = new Audio('music2.mp3');
var music2 = new Audio('music.mp3');
var volume = 0.4;
var waitkeys = false;
var tempmusicstate = false;
music.volume = localStorage.volume || volume;
music2.volume = localStorage.volume || volume;
music.loop = true;
music2.loop = true;

var flowerimg = new Image();
flowerimg.src = "flower.png";
var flowerimg2 = new Image();
flowerimg2.src = "flower2.png";

var arrowimg = new Image();
arrowimg.src = "arrow.png";

var flowersnd = new Audio("flower.wav");
flowersnd.load();
flowersnd.loop = false;
flowersnda = [ flowersnd, flowersnd.cloneNode(), flowersnd.cloneNode(), flowersnd.cloneNode() ];

function pause() {
	if (paused || draw != frameRun)
		return;
	pt = performance.now();
	paused = true;
	cvs.style.cursor = "";
}

// Keep track of keys.
window.onkeydown = function(e){
	e = e || window.event;
	pressed[e.code] = true; // So KeyW etc follow regardless of layout.
	pressed[e.key] = true; // Firefox reports arrow keys only this way, I think.
	if (e.repeat)
		return;
	if (e.key == 'Alt' || e.key == 'Control' || e.key == 'OS') // Skip alt etc so people can get away without disturbing the game.
		return;
	// Handle escape/anykey for pausing.
	if (paused == true) {
		st += performance.now() - pt; // Subtract pausing time from total.
		paused = false;
		cvs.style.cursor = "none";
	} else if (e.code == 'Escape' && draw == frameRun) {
		pause();
	}
	// Mute music with m key.
	if (e.which == 77) {
		var targetvol = (music.volume == 0) ? volume : 0;
		music.volume = targetvol;
		music2.volume = targetvol;
		localStorage.volume = targetvol;
		tempmusicstate = true;
		setTimeout(function() {
			tempmusicstate = false;
		}, 2000);
		return;
	}
	// Start the game when a key is pressed in pretty much every screen that is not running game.
	if (draw != frameRun && !waitkeys) {
		startgame(); // So offsets etc are right, ship flies for intro.
		draw = frameRun;
		cvs.style.cursor = "none";
		music2.pause();
		music.loop = true; // Done here because after end game we just stop looping instead of stop the music.
		music.currentTime = 0;
		music.play();
	}
}

window.onkeyup = function(e){
	if (e.repeat)
		return;
	e = e || window.event;
	delete pressed[e.code];
	delete pressed[e.key];
}

// Called on start and resize.
function init() {
	w = cvs.width = window.innerWidth;
	h = cvs.height = window.innerHeight;
	cx = w / 2;
	cy = h / 2;
	background0 = new Background("background.jpg", 0.5, 4);
	background1 = new Background("background2.png", 0.75, 2);
	background2 = new Background("background3.png", 0.9, 2.5);
}

// Called when gameplay starts.
function startgame() {
	player = new Player(player.a, player.s);
	enemies.forEach(function(e) {
		e.destroy();
	});
	enemies = [];
	enemycount = 10;
	flowercount = 69;
	flowersdone = 0;
	var espread = 100000;
	for (var i = 0; i < enemycount; ++i) {
		var ex = (Math.random() - 0.5) * espread;
		var ey = (Math.random() - 0.5) * espread;
		if (Math.sqrt(ex * ex + ey * ey) < 1000)
			--i; // Avoid enemies spawning too close to player.
		else enemies[i] = new Enemy(ex, ey, i);
	}
	var fspread = 50000;
	for (var i = 0; i < flowercount; ++i) {
		flowers[i] = new Flower((Math.random() - 0.5) * fspread, (Math.random() - 0.5) * fspread, i);
	}
	st = performance.now();
}

function endgame() {
	et = performance.now();
	music.pause();
	music2.currentTime = 0;
	music2.play();
	cvs.style.cursor = "";

	waitkeys = true;
	setTimeout(function () {
		waitkeys = false;
	}, 1000);
}

function frame(t) {
	//ctx.globalAlpha = 1; // TODO necessary?

	dt = t - tt;

	// Draw the background.
	background0.frame(t);
	background1.frame(t);
	background2.frame(t);

	draw(t);

	// Draw the ship.
	player.frame(t);

	// More frames!
	tt = t;
	window.requestAnimationFrame(frame);
}

function outlineText(text, size, align, x, y) {
	ctx.textAlign = align;
	ctx.fillStyle = "#FFFFFF";
	ctx.strokeStyle = '#000000';
	ctx.lineWidth = 2;
	ctx.font = "bold " + size + "px Courier New";
	ctx.strokeText(text, x, y);
	ctx.fillText(text, x, y);
}

function showMusicState() {
	if (music.volume == 0)
		outlineText("Music off.", 20, "left", 20, h - 20);
	else outlineText("Music on.", 20, "left", 20, h - 20);
}

function frameBegin(t) {
	// Show before game starts.
	outlineText("Pollin8r 9000", 50, "center", cx, cy - 80);
	outlineText("Press any key to start.", 30, "center", cx, cy + 80);
	outlineText("Use WASD or arrow keys to move (or ZQSD for azerty).", 20, "center", cx, cy + 120);
	outlineText("Visit all the flowers and avoid being taken by hornets.", 20, "center", cx, cy + 140);
	outlineText("Escape key pauses the game.", 20, "center", cx, cy + 160);
	outlineText("Press M to mute/unmute the music.", 20, "center", cx, cy + 180);
	showMusicState();
};

function frameWon(t) {
	frameRun(t);
	// Show when won.
	outlineText("You Win!", 50, "center", cx, cy - 80);
	if (!waitkeys)
		outlineText("Press any key to restart.", 30, "center", cx, cy + 80);
	outlineText("Visited all " + flowercount + " flowers in " + (Math.round((et - st) / 10) / 100) + " seconds.", 20, "center", cx, cy + 120);
	if (isBestTime) {
		if (localStorage.bestTimePrev !== undefined) {
			outlineText("Previous best time: " + (Math.round((localStorage.bestTimePrev) / 10) / 100) + " seconds.", 20, "center", cx, cy + 140);
			outlineText("You've beat the best time by " + (Math.round((localStorage.bestTimePrev - localStorage.bestTime) / 10) / 100) + " seconds!", 20, "center", cx, cy + 160);
		}
	} else if (localStorage.bestTime !== undefined)
		outlineText("Best time: " + (Math.round((localStorage.bestTime) / 10) / 100) + " seconds.", 20, "center", cx, cy + 140);
	showMusicState();
}

function frameLost(t) {
	frameRun(t);
	// Show when lost.
	outlineText("You've been taken!", 50, "center", cx, cy - 40);
	if (!waitkeys)
		outlineText("Press any key to restart.", 30, "center", cx, cy + 10);
	outlineText("Visited " + flowersdone + " out of " + flowercount + " flowers in " + (Math.round((et - st) / 10) / 100) + " seconds.", 20, "center", cx, cy + 50);
	showMusicState();
}

function frameRun(t) {
	var cf = 0, cfd = undefined;
	// Draw flowers and arrow.
	for (var i = 0; i < flowers.length; ++i) {
		var e = flowers[i];
		e.frame(t);
		var ex = e.x - player.x;
		var ey = e.y - player.y;
		var d = Math.sqrt(ex * ex + ey * ey);
		if (!e.done && (cfd === undefined || d < cfd)) {
			cfd = d;
			cf = i;
		}
	}
	if (cfd > 500 && player.die === false) {
		var e = flowers[cf];
		var ex = e.x - player.x;
		var ey = e.y - player.y;
		var a = Math.atan2(ex, ey);
		ctx.save();
		ctx.translate(cx, cy);
		ctx.rotate(a);
		ctx.drawImage(arrowimg, -50, -50 - 200, 100, 100);
		ctx.restore();
	}

	// Draw enemies.
	enemies.forEach(function(e) { e.frame(t); });

	// Draw some info.
	outlineText("Speed: " + (Math.round(player.s * 100) / 100), 20, "left", 10, 30);
	outlineText("Position: " + Math.round(player.x) + " " + Math.round(player.y), 20, "left", 10, 50);
	outlineText("Flowers visited: " + flowersdone + " / " + flowercount, 20, "left", 10, 70);
	//outlineText("Seconds elapsed: " + String(Math.round((performance.now() - st) / 10) / 100).padEnd(4, '0'), 20, "left", 10, 90);

	// Draw map/radar.
	var d = 100;
	var m = 50;
	var c = d + m;
	ctx.globalAlpha = 0.2;
	ctx.strokeStyle = "#FFFFFF";
	ctx.fillStyle = "#222222";
	ctx.beginPath();
	ctx.arc(w - c, c, d, 0, 2 * Math.PI);
	ctx.stroke();
	ctx.fill();
	ctx.globalAlpha = 1;
	ctx.fillStyle = "#FFFFFF";
	flowers.forEach(function(e) {
		var ex = (e.x - player.x) / 100;
		var ey = (e.y - player.y) / 100;
		if (Math.sqrt(ex * ex + ey * ey) > d) {
			var a = Math.atan2(ex, ey);
			ex = Math.sin(a) * d;
			ey = Math.cos(a) * d;
		}
		if (!e.done) {
			ctx.beginPath();
			ctx.arc(w - c - 1 + ex, c - 1 - ey, 2, 0, 2 * Math.PI);
			ctx.fill();
		}
	});
	ctx.fillStyle = "#FF9900";
	enemies.forEach(function(e) {
		var ex = (e.x - player.x) / 100;
		var ey = (e.y - player.y) / 100;
		if (Math.sqrt(ex * ex + ey * ey) > d) {
			var a = Math.atan2(ex, ey);
			ex = Math.sin(a) * d;
			ey = Math.cos(a) * d;
		}
		if (e.die === false) {
			ctx.beginPath();
			ctx.arc(w - c - 1 + ex, c - 1 - ey, 2, 0, 2 * Math.PI);
			ctx.fill();
		}
	});
	ctx.fillStyle = "#FFFF00";
	ctx.beginPath();
	ctx.arc(w - c - 1, c - 1, 2, 0, 2 * Math.PI);
	ctx.fill();

	// Show when paused.
	if (paused) {
		outlineText("Game Paused", 30, "center", cx, cy - 80);
		outlineText("Press any key to continue.", 20, "center", cx, cy + 80);
		showMusicState();
	} else if (tempmusicstate)
		showMusicState();
}

function Background(image, pf, bgsf) { // To change relative speed use pf.
	var bgimg = new Image();
	bgimg.src = image;

	this.frame = function(tm) {
		if (bgimg.width == 0)
			return;
		var iw = w * bgsf;
		var ih = iw / bgimg.width * bgimg.height;
		var dx = Math.abs(player.x * pf) % iw * Math.sign(player.x) * -1;
		var dy = Math.abs(player.y * pf) % ih * Math.sign(player.y);
		ctx.drawImage(bgimg, dx, dy, iw, ih);
		ctx.drawImage(bgimg, dx + iw, dy, iw, ih);
		ctx.drawImage(bgimg, dx - iw, dy, iw, ih);
		ctx.drawImage(bgimg, dx, dy + ih, iw, ih);
		ctx.drawImage(bgimg, dx + iw, dy + ih, iw, ih);
		ctx.drawImage(bgimg, dx - iw, dy + ih, iw, ih);
		ctx.drawImage(bgimg, dx, dy - ih, iw, ih);
		ctx.drawImage(bgimg, dx + iw, dy - ih, iw, ih);
		ctx.drawImage(bgimg, dx - iw, dy - ih, iw, ih);
	}
};

function Ship(ix, iy, ia, is, style) {
	this.frames = [ new Image(), new Image() ];
	this.frames[0].src = "player1.png";
	this.frames[1].src = "player2.png";
	this.sz = 100;

	this.x = ix; // Absolute position.
	this.y = iy;
	this.a = ia; // Angle.
	this.s = is; // Speed.
	this.dx = 0;
	this.dy = 0;
	this.die = false;
	this.flap = 0;

	this.draw = function(t, ex, ey) {
		ctx.strokeStyle = style;
		ctx.beginPath();
		if (this.die !== false && this.die !== true) {
			if (this.die == 0)
				kaboom();
			var xs = 0.2;
			for (var i = 0; i < 20; ++i) {
				ctx.moveTo(ex + Math.sin(Math.random() * Math.PI * 2) * Math.random() * this.die * xs, ey + Math.cos(Math.random() * Math.PI * 2) * Math.random() * this.die * xs);
				ctx.lineTo(ex + Math.sin(Math.random() * Math.PI * 2) * Math.random() * this.die * xs, ey + Math.cos(Math.random() * Math.PI * 2) * Math.random() * this.die * xs);
			}
			this.die += dt;
			ctx.globalAlpha = 1 - 1/1000 * Math.min(this.die, 1000);
			if (this.die >= 1000)
				this.die = true;
		} else if (this.die == false) {
			ctx.save();
			ctx.translate(ex, ey);
			ctx.rotate(-this.a);
			this.flap += dt * this.s;
			this.flap %= 400;
			ctx.drawImage(this.frames[this.flap > 200 ? 1 : 0], -this.sz / 2, -this.sz / 2, this.sz, this.sz)
			ctx.restore();
		}
		ctx.stroke();
		ctx.globalAlpha = 1;
		if (!paused) {
			this.x += this.dx = -Math.sin(this.a) * dt * this.s;
			this.y += this.dy = Math.cos(this.a) * dt * this.s;
		}
	}
}

function Enemy(x, y, id) {
	var max_spd = 2.5;
	Ship.call(this, x, y, Math.random() * Math.PI * 2, Math.random() * 0.5 + 1.5, "#FFFF00");
	this.id = id;
	this.dead = false;
	this.snd = new EnemySound();
	this.normalspeed = this.s; // Normal speed.
	this.frames[0].src = "enemy1.png";
	this.frames[1].src = "enemy2.png";
	this.sz = 150;
	this.arand = Math.random() * Math.PI * 2;
	this.rdist = Math.random() * 500;
	this.flower = undefined;
	this.frame = function(t) {
		var distx = this.x - player.x; // TODO how is this different from ex,ey,cx,cy?
		var disty = this.y - player.y;
		var dist = Math.sqrt(distx * distx + disty * disty);
		this.s = Math.min(this.normalspeed * (1 + dist / 1000000), max_spd);
		this.snd.setd(dist);
		if (dist < 30 && player.die === false && draw == frameRun) {
			//this.die = 0;
			player.die = 0;
			endgame();
			draw = frameLost;
		}

		var tx, ty;
		if (dist < 8000) { // Aim for a random point near the player if he is near.
			tx = player.x + Math.cos(player.a + this.arand) * this.rdist, ty = player.y + Math.sin(player.a + this.arand) * this.rdist;
			if (this.flower !== undefined)
				--this.flower.busy;
			this.flower = undefined;
		} else { // If not, go to the nearest flower.
			if (this.flower === undefined) {
				var fd = undefined;
				var mb = 0;
				for (var i = 0; i < flowers.length; ++i) {
					if (flowers[i].done)
						continue;
					var dx = this.x - flowers[i].x, dy = this.y - flowers[i].y;
					if (fd === undefined || (Math.sqrt(dx * dx + dy * dy) < fd && flowers[i].busy <= mb)) {
						fd = Math.sqrt(dx * dx + dy * dy);
						mb = flowers[i].busy;
						this.flower = flowers[i];
					}
				}
				if (this.flower !== undefined)
					++this.flower.busy;
			}
			if (this.flower !== undefined)
				tx = this.flower.x, ty = this.flower.y;
		}
		var ed = Math.atan2(this.x - tx, ty - this.y) - this.a;
		if (!paused) {
			if (Math.abs(ed) < Math.PI)
				this.a += ed / 150 * dt;
			else this.a += (ed - ((ed > 0) ? 1 : -1) * Math.PI * 2) / 150 * dt;
		}
		this.draw(t, cx + distx, cy - disty);
	}
	this.destroy = function() {
		this.snd.stop();
	}
}

function Flower(x, y, id) {
	this.id = id;
	this.sz = 100 + Math.random() * 50;
	this.done = false;
	this.busy = 0;

	this.x = x; // Absolute position.
	this.y = y;

	this.draw = function(t, ex, ey) {
		ctx.drawImage(this.done ? flowerimg2 : flowerimg, ex - this.sz / 2, ey - this.sz / 2, this.sz, this.sz)
	}

	this.frame = function(t) {
		var distx = this.x - player.x;
		var disty = this.y - player.y;
		var dist = Math.sqrt(distx * distx + disty * disty);
		if (dist < this.sz - 50 && !this.done && player.die === false) {
			this.done = true;
			flowersnda[flowersdone % flowersnda.length].play();
			++flowersdone;
			if (flowersdone == flowercount && draw == frameRun) {
				endgame();
				if (localStorage.bestTime === undefined || localStorage.bestTime >= (et - st)) {
					if (localStorage.bestTime !== undefined)
						localStorage.bestTimePrev = localStorage.bestTime;
					localStorage.bestTime = et - st;
					isBestTime = true;
				} else isBestTime = false;
				draw = frameWon;
			}
		}
		this.draw(t, cx + distx, cy - disty);
	}
}

function Player(a, s) {
	var max_fwd = 3, max_rev = 1.5;
	Ship.call(this, 0, 0, a, s, "#FFFF00");
	this.fire = false;
	this.frame = function(t) {
		vrr(this.s);
		if (draw != frameRun) {
			this.draw(t, cx, cy);
			if (draw != frameBegin)
				this.s -= this.s * (Math.min(dt, 5000) / 5000);
			else if (this.s > 0.5) this.s = Math.max(this.s - this.s * (Math.min(dt, 5000) / 5000), 0.5);
			return;
		}
		// Handle keys.
		if (!paused && draw == frameRun) {
			if (pressed['KeyD'] || pressed['ArrowRight']) // Right.
				this.a -= dt / 180;
			if (pressed['KeyA'] || pressed['ArrowLeft']) // Left.
				this.a += dt / 180;
			if (pressed['KeyW'] || pressed['ArrowUp']) { // Up.
				this.s += dt / 1000;
				if (this.s > max_fwd)
					this.s = max_fwd;
			} else this.s -= this.s * (Math.min(dt, 5000) / 5000);
			if (pressed['KeyS'] || pressed['ArrowDown']) { // Down.
				this.s -= dt / 1000;
				if (this.s < -max_rev)
					this.s = -max_rev;
			}
		}
		// Draw.
		this.draw(t, cx, cy);
	}
}

// Engine noise.
var vrr_osca = actx.createOscillator();
var vrr_oscb = actx.createOscillator();
var vrr_oscc = actx.createOscillator();
var vrr_gain = actx.createGain();
vrr_osca.type = 'sawtooth';
vrr_oscb.type = 'sawtooth';
vrr_oscc.type = 'square';
vrr_osca.connect(vrr_gain);
vrr_oscb.connect(vrr_gain);
vrr_oscc.connect(vrr_gain);
vrr_gain.connect(globalgain);
vrr_gain.gain.setValueAtTime(0, actx.currentTime);
vrr_osca.start();
vrr_oscb.start();
vrr_oscc.start();

function vrr(s) {
	vrr_osca.frequency.setValueAtTime(Math.abs(s) * 40, actx.currentTime);
	vrr_oscb.frequency.setValueAtTime(Math.abs(s) * 80, actx.currentTime);
	vrr_oscc.frequency.setValueAtTime(Math.abs(s) * 99, actx.currentTime);
	vrr_gain.gain.setValueAtTime(Math.abs(s) / 150, actx.currentTime);
}

// Enemy engine noise.
function EnemySound() {
	this.osca = actx.createOscillator();
	this.oscb = actx.createOscillator();
	this.gain = actx.createGain();
	this.osca.type = 'sawtooth';
	this.oscb.type = 'sawtooth';
	this.osca.connect(this.gain);
	this.oscb.connect(this.gain);
	this.gain.connect(globalgain);
	this.gain.gain.setValueAtTime(0, actx.currentTime);
	this.osca.frequency.setValueAtTime(420 + Math.random() * 10, actx.currentTime);
	this.oscb.frequency.setValueAtTime(219 + Math.random() * 10, actx.currentTime);
	this.osca.start();
	this.oscb.start();
	this.setd = function(d) {
		this.gain.gain.setValueAtTime(Math.min((2 / d), 0.4), actx.currentTime);
	}
	this.stop = function() {
		this.osca.stop();
		this.oscb.stop();
	}
}

// Make kaboom noise.
function kaboom() {
	var osc = actx.createOscillator();
	var gain = actx.createGain();
	osc.type = 'square';
	osc.connect(gain);
	gain.connect(globalgain);
	osc.start();
	for (var i = 0; i < 100; ++i) {
		osc.frequency.setValueAtTime(100 + Math.random() * 100, actx.currentTime + i / 100);
		gain.gain.setValueAtTime(0.2 - i * (0.2 / 50), actx.currentTime + i / 100);
	}
	osc.stop(actx.currentTime + 100 / 200);
}

function _start() {
	// Start the show.
	body = document.body;
	window.onresize = init;
	body.innerHTML = "";
	body.appendChild(cvs);
	body.style.margin = 0;
	body.style.overflow = "hidden";
	//cvs.style.cursor = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjbQg61aAAAADUlEQVQYV2P4//8/IwAI/QL/+TZZdwAAAABJRU5ErkJggg=='), url(images/blank.cur), none !important";
	//cvs.style.cursor = "none";
	init();
	startgame();
	window.requestAnimationFrame(frame);
}

document.addEventListener("visibilitychange", function () {
	if (document.hidden) {
		globalgain.gain.setValueAtTime(0, actx.currentTime);
		music.muted = true;
		music2.muted = true;
		pause();
	} else {
		globalgain.gain.setValueAtTime(1, actx.currentTime);
		music.muted = false;
		music2.muted = false;
	}
});
